name := "GuiSpider"
//name := "commandSpider"
version := "1.5"
scalaVersion := "2.12.1"

assemblyJarName in assembly := "runnable.jar"

lazy val commonSettings = Seq(
  libraryDependencies ++= {
    Seq(
      "org.scalactic" %% "scalactic" % "3.0.0",
      "org.scalatest" %% "scalatest" % "3.0.0" % "test",
      "org.jsoup" % "jsoup" % "1.8.3",
      "org.scala-lang.modules" %% "scala-swing" % "2.0.0-M2"
    )
  },
  test in assembly := {}
)

/*
lazy val spider = project
  .in(file("."))
  .enablePlugins(JavaServerAppPackaging)
  .settings(commonSettings: _*)
  .settings(
      name := "Spider",
      version := "0.0.1",
      mainClass in Compile := Some("spider.ImageDownloaderApp")
  )
*/


lazy val guiSpider = project
  .in(file("."))
  .enablePlugins(JavaServerAppPackaging)
  .settings(commonSettings: _*)
  .settings(
      name := "GuiSpider",
      version := "0.0.1",
      mainClass in assembly := Some("guiWrapper.SwingWrapper")
      //mainClass in Compile := Some("guiWrapper.SwingWrapper")
  )

/*
lazy val apiSpider = project
  .in(file("."))
  .enablePlugins(JavaServerAppPackaging)
  .settings(commonSettings: _*)
  .settings(
      name := "commandSpider",
      version := "0.0.1",
      mainClass in Compile := Some("spider.commands")
  )
*/
