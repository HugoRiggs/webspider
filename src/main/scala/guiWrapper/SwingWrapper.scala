package guiWrapper

import swing._
import swing.event._
import concurrent.ExecutionContext.Implicits.global
import util.{Success, Failure}
import java.awt.Color._ 
import spider._
import LoadingBar.symbol


object Timer {

  private var t : javax.swing.Timer = _

  def apply(interval: Int, repeats: Boolean = true)(op: => Unit) {
    val timeOut = new javax.swing.AbstractAction() {
      def actionPerformed(e : java.awt.event.ActionEvent) = op
    }
    t = new javax.swing.Timer(interval, timeOut)
    t.setRepeats(repeats)
    t.start()
  }

  def stop() = { t.stop() }

}

object SwingWrapper extends SimpleSwingApplication {

  lazy val progressBar = 
    new ProgressBar() {min=0;max=50; background = green; minimumSize = new Dimension(50,10)
    maximumSize = new Dimension(100,10) }
  
  def top = new MainFrame{

    // Gui init properties
    val d = new Dimension(1000,600)
    val d1 = new Dimension(900,500)
    val d2 = new Dimension(500, 40) 
    val d3 = new Dimension(40, 40)
    val d4 = new Dimension(500, 22)
    size=d 
    title = "scala webspider"
    // Buttons
    val startButton = new Button { text = "Start" }
    val clearButton = new Button { text = "Clear Text" }

    // Checkboxes
    val deadLinkCheckBox = new CheckBox { text = "Find Dead Links:" }
    val mediaCheckBox = new CheckBox { text = "Find Media Links:" }
    val titleCB = new CheckBox { text = "Find Titles:" }
    val downloadImagesCB = new CheckBox { text = "Download media:"; background = white }
    val externalCB = new CheckBox { text = "External Links:" }
    val jsCB = new CheckBox { text = "Scrape javascript for Additional URLs:" }

    // text components 
    val outputText = new TextArea { editable=false; }
    val helpLabel = new Label { text = "Regex links must satisfy (java-format)" }

    val urlL = new Label { text = "URL:" }
    val urlTF = new TextField { text = ""; maximumSize=d2 }

    val mediaRegexTF = new TextField { text = ".*"; maximumSize=d2 }
    val mediaRegexL = new Label { text = "Regex media must satisfy" }

    val iterationsL = new Label { text = "max iteration" }
    val iterationsTF = new TextField { text = "20"; maximumSize=d3 }

    val linkRegexL = new Label { text = "Regex links must satisfy (java-format)" }
    val linkRegexTF = new TextField { text = ".*"; maximumSize=d2  }

    val pathTF = new TextField {  maximumSize=d4; background = white  }
    val pathL = new Label { text = "Downloads path";  background = white  }

    // scroll pane for the outputText
    val sp = new ScrollPane(outputText){maximumSize=d1; preferredSize=d1; minimumSize=d1}

    contents = new BoxPanel(Orientation.Vertical) {
      contents += new BoxPanel(Orientation.Horizontal){
                background = magenta 
        contents += new BoxPanel(Orientation.Vertical) {
          background = yellow

          contents += urlL // Get url
          contents += urlTF

          contents += linkRegexL
          contents += linkRegexTF
          
          contents += mediaRegexL
          contents += mediaRegexTF 

          contents += pathL
          contents += pathTF

          contents +=iterationsL // get iterations 
          contents +=iterationsTF
        }

        contents += new BoxPanel(Orientation.Vertical) {
              background = white
          contents += new BoxPanel(Orientation.Horizontal){
            contents += startButton
            contents += clearButton 
          }
              contents += deadLinkCheckBox
              contents += mediaCheckBox
              contents += titleCB 
              contents += downloadImagesCB
              contents += externalCB
              contents += jsCB 
        }

        contents += new BoxPanel(Orientation.Vertical){
          contents += new BoxPanel(Orientation.Horizontal){
          }
          contents += new BoxPanel(Orientation.Horizontal){  }

          contents += new BoxPanel(Orientation.Horizontal){
            // check boxes
            contents += new BoxPanel(Orientation.Vertical){

            }
            contents += new BoxPanel(Orientation.Vertical){

            }
            contents += new BoxPanel(Orientation.Vertical){
              contents += new BoxPanel(Orientation.Horizontal){
                background = white
              }
            }
          }
        }
      }
        contents += progressBar 
        contents += sp // lower text field
        border = Swing.EmptyBorder(30, 30, 10, 10)
    }


    listenTo(startButton)
    listenTo(clearButton)

    reactions += {
      case ButtonClicked(b) => {
        if(b.text == "Start"){

          outputText.text += "\nCrawling this may take a while ... \n"

          var criteria = collection.immutable.Seq[String]()

          criteria = criteria :+ "href"
          criteria = criteria :+ "src"
          criteria = criteria :+ "source"
          criteria = criteria :+ "title" 

          if(jsCB.selected)
            criteria = criteria :+ "javascript";

          val critSeq = criteria.toSeq
          print("\nChosen criteria are: "+critSeq+"\n")
          outputText.text += "\nChosen criteria are: "+criteria.toString+"\n"
          val crawl = Crawler(urlTF.text, iterationsTF.text.toInt, linkRegexTF.text, mediaRegexTF.text, critSeq)

          Timer(200) { progressBar.value = crawl.loadingBar.currentFillLength }

          crawl.crawlExternal = if( externalCB.selected) true else false

          crawl.run.onComplete({
            case Success(res) =>
            {

              import crawl._
              outputText.text += "\niterations set to "+iterationsTF.text.toInt + "\n"

              if(deadLinkCheckBox.selected)
                outputText.text += deadLinkSet.mkString("\ndead links gathered: " + deadLinkSet.size + " [\n", "\n", "\n]\n")

              outputText.text += localSet.mkString("\nLinks gathered: " + localSet.size + " [\n", "\n", "\n]\n")

              if(titleCB.selected)
                for((k,v) <- metaDataMap) 
                  outputText.text += (k + " is " + v + "\n")

              if(mediaCheckBox.selected){
                outputText.text += mediaSet.mkString("\nmedia links gathered: " + mediaSet.size + " [\n", "\n", "\n]\n")
                if(downloadImagesCB.selected){
                  outputText.text +="\nStarting downloads ... "
                  mediaSet.foreach(img => ImageDownloader.download(img, pathTF.text, mediaRegexTF.text))
                  outputText.text += " Done downloading images\n"
                }
              }

              if(externalCB.selected)
                outputText.text += externalSet.mkString("\nExternal Links Gathered: " + externalSet.size + " [\n", "\n", "\n]\n")

            }

            case Failure(ex) => outputText.text += ex.toString + "\n"
          })


        } else if(b.text == "Clear Text"){
          outputText.text = ""
          Timer.stop()
          progressBar.value = 0
        }

      }
    }

  }
}
