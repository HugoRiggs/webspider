package subredditSuggester
/*
import Spider._
import concurrent.ExecutionContext.Implicits.global
import util.{Success, Failure}
import sys.process._
import java.io.File
import collection.mutable.ListBuffer


object SubRedditSuggestionsFor {
  new File("results.html")

  def gatherComments(userName: String): List[String] = {
    val c = Crawler("https://www.reddit.com/u/" + userName, 1, Seq("href", "src") : _*)
    c.run
  
 //   for(i <- 0 to 2){
 //     val nextPage = (c.toList filter((x: String) => x.contains(userName) && x.contains("after") && x.contains("count")))(i)
 //     Crawler(nextPage, 1, buffers, Seq("href", "src") : _*)
 //   }
 //   c.localBuffer.toList filter(x => (x.contains("/comments/") & !x.contains("/user/"))) 
  }

  def bing(phrase: String): String = {  // search on bing
    val init = """http://www.bing.com/search?q=site%3A+reddit.com+"""
    val url = init+phrase.replaceAll(" ", "+").mkString + "&count=100"
    s"curl $url" #> new File("results.html") !!
  }

  def yahoo(phrase: String): String = { // search on Yahoo 
    val init =      """https://search.yahoo.com/search?p=site%3Areddit.com+"""
    val url = init+phrase.replaceAll(" ", "+").mkString + "&n=100"
    s"curl $url" #> new File("results.html") !!
  }

  def combinationFrom(s: List[String], n: Int): String = {
    (for(i <- 0 until 4) yield s( util.Random.nextInt(s.size)).split("/r/")(1) )
      .distinct.mkString(" ")
  }

  def urlFrom(s: String): String  =  s.split("/comments/")(0)

  def apply(userName: String) = {
    require(userName.length > 0, "enter a reddit user name")
    val t0 = System.nanoTime

    val comments = gatherComments(userName)

    val theirSubreddits = (for( c <- comments) yield urlFrom(c)).distinct
    bing( combinationFrom(theirSubreddits, 6) )
    yahoo( combinationFrom(theirSubreddits, 6) )
    import org.jsoup.Jsoup
    val doc = Jsoup.parse(new File("results.html"), "UTF-8", "https://bing.com/")
    val itr = doc.select("a[href]").iterator
    val b = ListBuffer()

    while(itr.hasNext) {
      val lnk = itr.next.attr("abs:href")
        if(lnk.contains("/r/"))
          try{
            b+=(lnk.substring(0, lnk.indexOf('/', 25)))
          } catch {case ex : Exception => println }
    }

    println("try these subreddits")
    b.toList.distinct.filterNot(theirSubreddits.toSet).foreach(x =>  println(x))

    val t1 = System.nanoTime

    print(s"\nThis program took "+(t1-t0)/1000000000+" seconds\n")
  }



}
*/
