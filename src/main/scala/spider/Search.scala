package spider

/***
* Search class and companion object.
* Use Jsoup to scrape key data from URL, such as media and links.
*/


import org.jsoup.Jsoup
import org.jsoup.nodes._
import scala.concurrent.{Future, Promise}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.annotation.tailrec


object Search  {
  def apply(url: String) = new Search(url, 20000)
  def apply(url: String, connectionTimeout: Int) = new Search(url, connectionTimeout)
}


class Search(url : String, connectionTimeout: Int) {

    val doc = Jsoup.connect(url)
      .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/49.0.2")
      .referrer("http://www.google.com")
      .timeout(connectionTimeout)
      .ignoreContentType(true)
      .get

      def attr(el: Element, a: String): String = el.attr(a)
      def toS(el: Element, a: String): String = el.toString

    @tailrec private def accumulate(
      list : List[String], 
      itr : java.util.Iterator[org.jsoup.nodes.Element],
      a: String
    )(f: (Element, String) => String): List[String] = {
      if(! itr.hasNext )
        list
      else {
        val element = f(itr.next, a)
        accumulate(element :: list, itr, a)(f)
      }
    }


    def producer(criteria: String) = Future {
      val p = Promise[List[String]]()
      val f = p.future
      val c: String = criteria.toLowerCase
      val process: List[String] = {
        val r = 
          c match {
            case "href" => 
               accumulate(List(), doc.select("a[href]").iterator, "abs:href" )(attr)
            case "src" => 
               accumulate(List(), doc.select("[src]").iterator, "abs:src" ) (attr)
            case "source" => 
               accumulate(List(), doc.select("source").iterator, "abs:src" ) (attr)
            case "video" => 
               accumulate(List(), doc.select("video").iterator, "abs:src" ) (attr)
            case "javascript" =>
              import scala.collection.JavaConversions._
               val lb = collection.mutable.ListBuffer[String]()
               val elements = doc.getElementsByTag("script").iterator
               elements.foreach(el => el.dataNodes.foreach(dn => lb += dn.getWholeData))
               lb.toList.distinct
            case anyTag => 
               accumulate(List(), doc.select("["+anyTag+"]").iterator, "abs:html" )(attr)
          }

        r  // results value
    }
    p success process
  }

}
