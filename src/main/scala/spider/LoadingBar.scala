package spider

/***
* Hugo Riggs
*
* sizeOfBar dashes necessary to fill bar
* ------------------------------------
*/

import StrictMath.{round, abs, floor}

abstract class Message 
case class LoadingBarMessage(msg: String) extends Message

object LoadingBar {
  def apply(size: Int) = new LoadingBar(size)
    val symbol = "="
}

class LoadingBar(size: Int) {
  import LoadingBar.symbol
  private val sizeOfBar:Int = 50 
  val b = sizeOfBar.toDouble / size.toDouble toInt
  val unit : Int = floor(sizeOfBar.toDouble / size.toDouble) toInt
  val remainder = floor(sizeOfBar.toDouble % size.toDouble) toInt
  val spc = " " * sizeOfBar
  val bck = "\b" * (sizeOfBar + "]100%".size)
  val str  = symbol * unit 

  var addedRemainder = false
  var currentFillLength = 0


  def start: String = "\n0["+spc+"]100%"+bck


  def updateBar(progress: Int): String = {
    val bk = "\b" * currentFillLength
    currentFillLength = progress * b 
    val sp = symbol * currentFillLength 
    bk+sp
  }

  def complete: String = {
    val bk = "\b" * currentFillLength
    val flBr = symbol * sizeOfBar
    currentFillLength = 50
    bk+flBr
  }

  def status: String = {

    val bk = "\b" * (currentFillLength + "0[".size)
    val sp = symbol * (currentFillLength + "0[".size)
    val rem = " " * (sizeOfBar - currentFillLength)

    bk+"0["+sp+rem+"]100%"
  }
}
