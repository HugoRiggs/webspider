package spider

import annotation.tailrec

object UserInput {

  @tailrec def choice(prompt: String)(action: => Any) : Unit = {
    print(s"\n$prompt (y, n)\n")
    val input = readLine()
    if(input.toLowerCase == "y"){
      action
    } else if(input.toLowerCase == "n") Unit
    else
      choice(prompt){action}
  }

}

object main extends App {

  args.foreach(arg => println(arg))
}
