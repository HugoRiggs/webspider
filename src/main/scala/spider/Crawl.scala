package spider

/***
* Crawler class and companion object.
* Recursively create search instances for 
* parsing data, namely URl's from visited web pages.
*/

import concurrent.ExecutionContext.Implicits.global
import concurrent.{Future}
import scala.util.{Success, Failure}
import collection.mutable

case object href
case object src 
case object source 
case object video 
case object title 

object Crawler {

  def help = print("Help: \nCreate two buffer objects in clien't code for accessing crawled links\n")
  val domainRegex = """^(?:https?:\/\/)?(?:[^@\/\n]+@)?(?:www\.)?([^:\/\n]+)""".r
  def getDomainFromURL(url: String) = {
    domainRegex.findFirstIn(url) match {
      case Some(n) => n
      case None => println("Error getting domain from url"); url
    }
  }
  def apply(url : String, criterias: Seq[String]) = { new Crawler(getDomainFromURL(url), url, -1, ".*", ".*", criterias : _*) }

  def apply(url : String, itr: Int, criterias : Seq[String]) = { new Crawler(getDomainFromURL(url), url, itr, ".*", ".*",criterias : _*) }

  // Limited, with regex filters on links
  def apply(url : String, itr: Int,regex: String, criterias : Seq[String]) = { new Crawler(getDomainFromURL(url), url, itr, regex, ".*", criterias : _*) }

  // Limited, with regex filters on links, and media
  def apply(url : String, itr: Int,regex: String, mediaRegex: String, criterias : Seq[String]) = { new Crawler(getDomainFromURL(url), url, itr, regex, mediaRegex, criterias : _*) }

  // Unlimited, with regex filters on links 
  def apply(url : String,regex: String, criterias : Seq[String]) = { new Crawler(getDomainFromURL(url), url, -1, regex, ".*", criterias : _*) }

  // Unlimited, with regex filters on links, and media
  def apply(url : String, regex: String, mediaRegex: String, criterias : Seq[String]) = { new Crawler(getDomainFromURL(url), url, -1, regex, mediaRegex, criterias : _*) }

}

  // Pass URL to parse, and number of iterations.
  // More iterations, means more hyperlinlks followed
  // from the originally supplied URL.
class Crawler(domain: String, url: String, iterations : Int, regex: String, mediaRegex: String, criterias : String*)  { // regex=*
require(url.length > 0 & (iterations < 5000 & iterations > 0) & criterias.size > 0, 
    "\nCrawler received wrong arguments:\n"
    + s"${url.length} = url.length" + "\n"
    + s"${iterations} = iterations" + "\n"
    + s"${criterias.size} = criterias.size" + "\n"
  ) 

  def printv(s: String) = if(verbose) print(s) else Unit

  val loadingBar = LoadingBar(iterations)

  val localSet, externalSet, deadLinkSet, mediaSet, jsSet, mediaSearchTraceSet = 
    mutable.Set[String]()

  val metaDataMap = collection.mutable.Map[String, String]()

  var verbose = false
  var crawlExternal: Boolean = true 
  val r = regex.r
  val mr = mediaRegex.r

  localSet+=url

  private def crawl(url: String, i: Int=0, i0: Int=0) : Int = {
    printv( loadingBar.updateBar(i) )

    def fillSets(search: Search) = {

      // Regex must be fufilled in any links we continue to search on
      def binaryAddLink(x:String):Unit = {
        if(x.contains(domain) && r.findAllIn(x).size>0) // & satisfies regex
            localSet+=x
          else if(r.findAllIn(x).size>0) // if satsifies regex
            externalSet+=x
      }

      def binaryAddMedia(x: String):Unit = {
        if(mr.findAllIn(x).size>0){
          mediaSet+=x
          mediaSearchTraceSet += "TRACE: " + url +" > " + x 
        }
        else
          Unit
      }

      // Go through all criteria
      for(c <- criterias) {

        // Check with the producer in search for a criteria
        search.producer(c).onComplete {
          case Success(result) =>

          // Match the promised value 
            result.future.value match {

              // s is a list of discovered URLs
              case Some(s) => 
                val vl = s.get

                // match the list to its appropriate subset, based on search criteria
                c match {

                  case "href" =>   // Add an href URL
                    vl foreach (x => binaryAddLink(x))

                  case "javascript" =>  // Add an URL found in script
                   val reg = """http[^'|^"]*""".r 
                    vl.foreach( e => {  
                          val filter1 = reg.findAllIn(e)
                          while(filter1.hasNext){
                            val str = filter1.next
                            jsSet+=(str)
                            val mdiaRgx = """([^\s]+(\.(?i)(jpg|png|gif|bmp|avi|wmv|flv|mpg|mp4)))""".r 
                            mdiaRgx.findFirstIn(str) match {
                              case Some(n) => binaryAddMedia(str)
                              case None => binaryAddLink(str)
                            }
                          }
                        } 
                    )

                  case "src" =>      // add media source
                    vl foreach (x => binaryAddMedia(x))

                  case "source" =>      // add media source
                    vl foreach (x => binaryAddMedia(x))

                  case "video" =>      // add media source
                    vl foreach (x => binaryAddMedia(x))

                  case "title" =>        // add title 
                    metaDataMap += ((s"title at ($url)") -> search.doc.title)

                }
            case None => printv("\nSearch was successful but completed with no result.")
          }
          case Failure(ex) => printv("\n"+ex + " failed on link " + url)
        }
      }
    }

    // If search succeeds, match data to a set, and if it fails add the link to the dead set.
    try{ 
      fillSets(Search(url))
    } catch { case ex : Exception => deadLinkSet+=url } // add dead link

    // Base case 
    if(i >= localSet.size || (i >= iterations && iterations != -1)) {
         printv( loadingBar.complete )
         0
    } else {      

      // Convert local links into array for later indexing
      val ary = localSet.toArray

      if(crawlExternal && externalSet.size > i0){

        val a = externalSet.toArray
        try{ 
          fillSets(Search(a(i0)))
        } catch { case ex : Exception => deadLinkSet+=url } // @TODO better exeption handling here (can't necessarily add to deadLinks, could be out of index)
        crawl(ary(i), i+1, i0+1)
      } else{

        crawl(ary(i), i+1, i0)
      }
    }
  }

  def run: Future[Int] =  Future {  
    printv( loadingBar.start )
    val t0 = System.nanoTime
    val r = crawl(url, 0, 0) 
    val t1 = System.nanoTime
    printv(s"\nDone! (buffer objects filled)\nThe crawl took "+(t1-t0)/1000000000+" seconds\n")
    r
  }
}
