package spider

/***
*       Image downloader object can be used to start a crawl which downloads media.
*       Image downloader app is more accessible to java as it extends App trait.
*
*	Download images from supplied URL and all hyperlinks found.
*       This object creates a crawl instance to gather more images. 
*	Note: sites like reddit, which have a 2 second delay per request 
*       may block. If crawling a site protected as such is your goal, 
*       create a script which sleeps and qualifies to the sites chrono-request standards.
*/


import concurrent.ExecutionContext.Implicits.global
import java.io.File

object ImageDownloader  {

  val alreadyDownloaded = collection.mutable.ListBuffer[String]()

  def getListOfFilesNames(dir: String):List[String] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList.map(file => file.getName)
    } else {
          List[String]()
    }
  }


  def download(media : String, path: String = "", reg: String = ".*") = {
    try {

      alreadyDownloaded ++= getListOfFilesNames(path)

      import sys.process._
      import java.net.URL


      val imageRegex = """([^\s]+(\.(?i)(jpg|png|gif|bmp)))""".r 
      /***
      * added `mp4` alone, as some files don't show extention e.i. (`video=mime\mp4?=20`) also added php
      * also added php, as some direct links to media files have no extention other than php in their URI
      */
      val videoRegex = """([^\s]+(\.(?i)(avi|wmv|flv|mpg|mp4)|(mp4)|(php)))""".r  // 
      val domainRegex = """^(?:https?:\/\/)?(?:[^@\/\n]+@)?(?:www\.)?([^:\/\n]+)""".r
      val extRegex = """(?i)(avi|wmv|flv|mpg|mp4|jpg|png|gif|bmp)""".r

      def url(media: String) = new URL(media)
      def name(url: URL) = {
        val n = url.getFile.replaceAll("""(\\\\|/|<|>|\?|\||\\|\||\.|\:|")+""", "_")
          if(n.length < 120)
            n     
          else
            n.substring(6, 96)
      }

      val ext = extRegex.findFirstIn(media) match {
          case Some(n) => "."+n
          case None => ".unkown"
      }

      val pathFixed = {
        val osName = System.getProperty("os.name")
        println(osName)
        osName match {
          case "Linux" => if(path.last=="/") path else path+"/"
          case "Windows 7" => if(path.last=="\\") path else path+"\\"
          case _ => print("Warning unsupported operating system.\n"); 
        }
      }

      imageRegex.findFirstIn(media) match {
        case Some(n) =>
            val u = url(media)
          if(reg.r.findFirstIn(media).size>0 && !alreadyDownloaded.contains(u.getPath)){
            print("\ndownloading: " + media + "\n")
            u #> new File(pathFixed + name(u) + ext) !!
            ;alreadyDownloaded += u.getPath
          }
        case None => Unit
      }

      videoRegex.findFirstIn(media) match {
        case Some(n) =>
          if(reg.r.findFirstIn(media).size>0){
            print("\ndownloading: " + media + "\n")
            def retryLoop(c: Int): Unit = {
              import org.jsoup.Jsoup
              val js = Jsoup.connect(media).ignoreContentType(true).userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(10000).get
              val absoluteURI =  js.baseUri
              val u = url(absoluteURI)
              val ext = extRegex.findFirstIn(absoluteURI) match {
                case Some(n) => "."+n
                case None => ".unknown"
              }
              try {
                if(c < 5 && !alreadyDownloaded.contains(u.getPath)){ u #> new File(pathFixed + name(u) + ext) !!
                 ;alreadyDownloaded+=u.getPath
                } else
                  Unit
              } catch {
                case ex: Exception => 
                  print("\ntrying again...\n")
                  retryLoop(c+1)
                }
              }
              retryLoop(0)
          }
        case None =>  Unit
      }

    } catch {
      case ex: Exception => println("Couldn't download from " + media)
    }
  }

  def apply(url : String, i: Int) = {
    val crawl = Crawler(url,  i, Seq("href", "src") )
      crawl.run.onSuccess({
        case 0 =>
          if(crawl.mediaSet.size > 0)
            crawl.mediaSet.toList foreach (s => ImageDownloader.download(s))
      })
  } 
}


object ImageDownloaderApp extends App  {
  require(args.length == 2, "\n\nUsage: imagedownloader url iterations\n\n")
  ImageDownloader(args(0), args(1).toInt)
}
