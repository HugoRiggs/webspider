package spider

/***
  * Dead link app 
  *     
  */

import concurrent.ExecutionContext.Implicits.global
import util.{Success, Failure}

object DeadLinkLister {
  def apply(url: String) = {
    val crawl = Crawler(url, 20, Seq("href", "src"))
    crawl.run.onComplete({
      case Success(res) =>
        import crawl._
        println( deadLinkSet.mkString("\ndead links gathered: " + deadLinkSet.size + " [\n", "\n", "\n]"))
        println( localSet.mkString("\nLinks gathered: " + localSet.size + " [\n", "\n", "\n]"))
      case Failure(ex) => println(ex)
    })
  }
}

object DeadLinkListerApp extends App {
  require(args.length == 1, "\nUsage: deadLinkChecker domain\n")
  DeadLinkLister(args(0))
}

