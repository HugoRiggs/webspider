
/***
  * If you want to check for domain name extentions the full list is here:
  * http://www.iana.org/domains/root/db
  *
  */

import org.scalatest.FunSuite
import concurrent.ExecutionContext.Implicits.global
import scala.util.{Success, Failure}


class SetSuite extends FunSuite {

//  test("custom entry test") {
//    import spider._
//      def loop(): Unit = {
//        print("url expected to be found: ")
//        val ts = io.StdIn.readLine
//        print("url: ")
//        val url = io.StdIn.readLine
//        print("max-iterations: ")
//        val itr = io.StdIn.readLine.toInt
//        val crawl = Crawler(url, itr, Seq("href", "src", "source", "video"))
//        import crawl._
//        crawl.run.onComplete({
//          case Success(res) => println(res)
//            println("links:")
//            mediaSet.toList foreach (s => println(s))
//
//            println("found link in media " + mediaSet.contains(ts))
//            println("found link in ext  " + externalSet.contains(ts))
//            println("found link in ext  " + localSet.contains(ts))
//
//
//            println("media links:")
//            mediaSet.toList foreach (s => println(s))
//
//            println("external links:")
//            externalSet.toList foreach (s => println(s))
//          case Failure(ex) => println(ex)
//        })
//
//      print("do another crawl?(y,n)")
//      val r = io.StdIn.readLine
//      if(r=="y")
//        loop
//      else if (r!="n")
//        loop
//      else 
//        Unit
//      }
//
//    loop
//  }
// @TODO These hang for a long time, then complete 
//  test("test rec script") {
//    import spider._
//      val crawl = Crawler("https://www.reddit.com/", 1, Seq("href", "src", "title") : _*)
//      import crawl._
//      crawl.run.onComplete({
//        case Success(res) => println(res)
//          println("media links:")
//          mediaSet.toList foreach (s => println(s))
//          println("Title:")
//          for((k,v) <- metaDataMap) {
//            println(k + ", " + v)
//          }
//        case Failure(ex) => println(ex)
//      })
//  }
//
//  test("test crawl githubio") {
//    import spider._
//      val crawl = Crawler("http://hugo-riggs.github.io/", 10, ".*resume", Seq("href", "src", "title") : _*)
//      crawl.run.onComplete({
//        case Success(res) => println(res)
//          import crawl._
//          println("media links:")
//          mediaSet foreach (s => println(s))
//          println("Title:")
//          for((k,v) <- metaDataMap) {
//            println(k + ", " + v)
//          }
//        case Failure(ex) => println(ex)
//      })
//}

  test("test crawl watch cartoon online, for video") {

    import spider._
    import concurrent.Await
    import concurrent.duration._

    val cartoonSpider = 
      Crawler("https://www.watchcartoononline.io/anime/family-guy-season-10"
      , 1, ".*(?i)family-guy-season-.*-episode-.*-.*"
      , Seq("href", "src",  "source"))

    cartoonSpider.verbose = true
    cartoonSpider.crawlExternal = true 


    cartoonSpider.run.onComplete({
      case Success(some) =>

        import cartoonSpider._

        println("local set size " + localSet.size)

        localSet.foreach (s => {

            //  use found episode links
            val episodeSpider = Crawler(s, 2, ".*(?i)family.*guy.*"
              , ".*(?i)family.*guy.*(flv|mp4).*", Seq("href", "src", "video", "javascript")) //(?i)family.*guy.*mp4 //(flv|mp4|video)

            // init values
            episodeSpider.verbose =  false 
            episodeSpider.crawlExternal = true

            // Now show media linnks
            println("Running crawl on: "  + s)

            episodeSpider.run.onSuccess {
              case n: Any => 
                episodeSpider.mediaSearchTraceSet.foreach(s=>println(s))
            }
          }
        )

        assert(localSet.contains("https://www.watchcartoononline.io/family-guy-season-10-episode-23-internal-affairs-2") == true)
        assert(localSet.contains("https://www.watchcartoononline.io/family-guy-season-10-episode-22-family-guy-viewer-mail-2-2") == true)

        //assert(localSet.contains("") == true)

        assert(localSet.contains("https://www.watchcartoononline.io/family-guy-season-10-episode-4-stewie-goes-for-a-drive-2") == true)
        assert(localSet.contains("https://www.watchcartoononline.io/family-guy-season-10-episode-3-screams-of-silence-the-story-of-brenda-q-2") == true)
        assert(localSet.contains("https://www.watchcartoononline.io/family-guy-season-10-episode-2-seahorse-seashell-party-2") == true)
        assert(localSet.contains("https://www.watchcartoononline.io/family-guy-season-10-episode-1-powerball-fever-2") == true)


       // mediaSet foreach (s => if(s.contains("mp4")) ImageDownloader.download(s) else Unit)
      case Failure(ex) => println("1" + ex)
    })

    // wait for link spider
    Await.result(cartoonSpider.run, 120.00 seconds)
    io.StdIn.readLine()
  }


//  test("image downloader "){
//    import spider._
//    val link = "http://imgur.com/r/cats"
//    
//      spider.ImageDownloader(link, 1)
//    }
//  }
// @TODO edit above

//  test("dead link checker ") {
//    import spider._
//    val link = "http://www.petstopdoc.com/buildBucket/"
//    val crawl = Crawler(link, 20, Seq("href", "src", "title") )
//      import crawl._
//      crawl.run.onComplete({
//        case Success(res) => 
////          println(res)
////          println("local link buffer:")
////          localSet foreach println
//
//            println("dead link buffer:")
//            deadLinkSet foreach println
//            assert(deadLinkSet.exists(url => url.contains("empty") ==  true && deadLinkSet.size >0))
//          case Failure(ex) => println(ex)
//      })
//    io.StdIn.readLine // todo await
//  }


/*
  test("crawl imgur ") {
    import spider._
    val imgurCrawl = 
      Crawler("http://imgur.com/r/gifs", 10, ".*", """.*\.gif""", Seq("href", "src", "source") : _*)
    imgurCrawl.run.onComplete({
      case Success(res) => 
        import imgurCrawl._
        assert(mediaSet.forall(media => """(?i)\.gif|\.mp4""".r.findAllIn(media).size>0 == true))
        assert(mediaSet.forall(media => """(?i)\.jpg|\.png""".r.findAllIn(media).size==0 == true))
        case Failure(ex) => println(ex)
    })
  }
 */ 
 //   
 //     println( bfr.toList.mkString("\nlocal links gathered: " + bfr.size+ " [\n", "\n", "\n]"))
 //     println( bfrExt.toList.mkString("\nexternal links gathered: " + bfrExt.size+ " [\n", "\n", "\n]"))
 //     println( bfrMedia.toList.mkString("\nmedia gathered: " + bfrMedia.size + " [\n", "\n", "\n]"))
 //   }
 // }

 // test("search for to get info"){
 //   import spider._
 //   
 //     val doc = new Search("http://www.petstopdoc.com/buildBucket/index.php", "links").doc
 //     println("title:\n" + doc.title)
 //     println("head:\n" + doc.head)
 //   }
 // }

 // 

//  test("test swing gui ") {
//      import spider._
//      import guiWrapper._
//      SwingWrapper.main(Array(""))
//      UserInput.choice("end test?"){ }
//  }

}
